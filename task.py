#!/usr/bin/python2

from stretch import DefaultEntry, insert_to_elastic, set_timezone
from datetime import datetime
from dateutil import parser as datetime_parser
        
task_mapping = '''
{  
  "mappings":{  
    "task":{  
        "properties": {
          "task": {
            "type": "string",
            "index": "not_analyzed"
          },
          "task_analyzed": {
            "type": "string"
          },
          "additional_info": {
            "type": "string"
          },
          "start": {
            "type": "date",
            "format": "strict_date_optional_time||epoch_millis"
          },
          "end": {
            "type": "date",
            "format": "strict_date_optional_time||epoch_millis"
          },
          "length": {
            "type": "double"
          },
         "complete": {
            "type": "boolean"
          },
          "timestamp": {
            "type": "date",
            "format": "strict_date_optional_time||epoch_millis"
          }
        }
    }
  }
}'''


class Task(DefaultEntry):
    index = 'task_v3'
    doc_type = 'task'
    entry_leftover_keys = (u'timestamp', u'length', u'task_analyzed')

    def __init__(self, task_name, start, end, complete=True, additional_info=""):
        super(Task, self).__init__(index=self.index, doc_type=self.doc_type)
        self['body']['task'] = task_name
        self['body']['task_analyzed'] = task_name
        self['body']['additional_info'] = additional_info
        self['body']['start'] = set_timezone(start)
        self['body']['end'] = set_timezone(end)
        self['body']['length'] = (end - start).total_seconds()/60.0
        self['body']['complete'] = complete
        self['body']['timestamp'] = set_timezone(end)

    @property
    def mapping(self):
        return task_mapping

    @classmethod
    def from_entry_dict(cls, dictionary):
        dictionary = dictionary.copy()
        task = cls(dictionary.pop('task'), 
            datetime_parser.parse(dictionary.pop('start')),
            datetime_parser.parse(dictionary.pop('end')),
            dictionary.pop('complete'),
            dictionary.pop('additional_info'))
        if tuple(dictionary.keys()) != tuple(cls.entry_leftover_keys):
            print 'WARNING: Unexpected entry format. \nEntry remaining keys: ("%s"), Expected entry remaining keys: "%s"' % (dictionary.keys(), cls.entry_leftover_keys)
        return task


def create_task(task_name, start, end, complete=True, additional_info=""):
    task = Task(task_name, start, end, complete, additional_info)
    return insert_to_elastic(task)
    

def get_from_user():
    print ""
    task_name = raw_input('(Press enter after writing the name to start working on the task)\ntask name: ')
    while task_name.strip() == '':
        print 'Requires a value for task name.'
        task_name = raw_input('task name: ')
    start = datetime.now()
    print start.isoformat()
    raw_input('Press to move on.')
    end = datetime.now()
    print end.isoformat()
    additional_info = raw_input('additional info[None]: ')
    while True:
        complete = raw_input('complete? [Y/N]: ')
        if complete.lower() in ('no', 'n'):
            complete = False
            break
        elif complete.lower() in ('yes', 'y'):
            complete = True
            break
    create_task(task_name, start, end, complete, additional_info)


if '__main__' == __name__:
    get_from_user()

