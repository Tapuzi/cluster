#!/usr/bin/python
import task
import code

from datetime import datetime as dt
from datetime import timedelta as td

from task import create_task as ct

print "sample usage: ct('taskname', dt(2016,8,2,10,32), dt(2016,8,2,10,32)+td(minutes=2), complete=True, additional_info='Create a helper script')"

print "(dt == datetime.datetime, td = datetime.timedelta, ct = task.create_task)"

code.interact(local=locals())
