#!/usr/bin/python2

from stretch import DefaultEntry, insert_to_elastic

def split_string(string, delimiters=(',', ' ')):
    options = [string]
    for delimiter in delimiters:
        new_options = []
        for option in options:
            new_options.extend(option.split(delimiter))
        new_options = (list(map(lambda o: o.strip(), new_options)))
        options = new_options
    return options
        
class Bookmark(DefaultEntry):
    def __init__(self, url, description, tags=None, personal=False):
        if personal:
            index = 'bookmark_me'
        else:
            index = 'bookmark_raycatch'
        doc_type = 'bookmark'
        super(Bookmark, self).__init__(index = index, doc_type=doc_type)
        self['body']['url'] = url
        self['body']['description'] = description
        if tags is not None:
            if isinstance(tags, basestring):
                tags = split_string(tags)
            if not isinstance(tags, dict):  # if tags is a list like object
                new_tags = {}
                for item in tags:
                    new_tags[item] = 1
                tags = new_tags
            for tag, strenth in tags.items():
                self['body'][tag] = strenth


def create_bookmark(url, description, tags=None, personal=False):
    bookmark = Bookmark(url, description, tags)
    return insert_to_elastic(bookmark)


def get_from_user():
    url = raw_input('URL: ')
    if url.strip() == '':
        raise RuntimeError('Requires a value for url.')
    description = raw_input('description: ')
    if description.strip() == '':
        raise RuntimeError('Requires a value for description.')
    tags = raw_input('tags[None]: ')
    if tags.strip() == '':
        tags = None
    while True:
        personal = raw_input('personal[False]: ')
        if personal.lower() in ('f', 'false', 'no', ''):
            personal = False
            break
        elif personal.lower() in ('t', 'true', 'yes'):
            personal = True
            break
    create_bookmark(url, description, tags, personal)


if '__main__' == __name__:
    get_from_user()

