#!/usr/bin/python2
import stretch, task

def migrate(entry_type=task.Task, from_index="task_v2"):
    inserted = 0
    try:
        res = stretch.elastic_client.search(index=from_index, body={"query": {"match_all": {}}}, size=1000)
        for hit in res['hits']['hits']:
            t = entry_type.from_entry_dict(hit['_source'])
            # print "Inserting: %s" % str(t)
            stretch.insert_to_elastic(t)
            inserted += 1
    finally:
        print("Inserted %d entries." % inserted)

