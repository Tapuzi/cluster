#!/usr/bin/python2

from datetime import datetime
from elasticsearch import Elasticsearch
from tzlocal import get_localzone
elastic_client = Elasticsearch(['http://localhost:9200'])


class DefaultEntry(dict):
    def __init__(self, index='default_index', doc_type=None, id=None):
        super(DefaultEntry, self).__init__()
        self['index'] = index
        if doc_type is not None:
            self['doc_type'] = doc_type
        if id is not None:
            self['id'] = id
        timezone = get_localzone()
        self['body'] = {'timestamp': set_timezone(datetime.now())}

    @property
    def mapping(self):
        return ''

        
def insert_to_elastic(entry):
    if entry.mapping != '':
        elastic_client.indices.create(index=entry.index, ignore=400, body=entry.mapping)
    res = elastic_client.index(**entry)
    return res


def set_timezone(dt, timezone=None):
    """Try to set timezone (default is local) to a datetime object. Will fail if it is already set."""
    if timezone is None:
        timezone = get_localzone()
    try:
        return timezone.localize(dt)
    except ValueError as exception:
        if "tzinfo is already set" in exception.message:
            return dt
        raise

def delete_index(index_name):
    return elastic_client.indices.delete(index=index_name, ignore=[400, 404])

