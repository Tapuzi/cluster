pip install -r requirements.txt
docker run -d --name itay_elastic -v "/opt/elasticsearch/esdata":/usr/share/elasticsearch/data -p 9200:9200 elasticsearch
docker run -d --name itay_kibana --link itay_elastic:elasticsearch -p 5601:5601 kibana

